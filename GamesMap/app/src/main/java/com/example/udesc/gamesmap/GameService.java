package com.example.udesc.gamesmap;

import java.util.ArrayList;

public class GameService {

    private DAOGame dao;

    public GameService() {
        dao = new DAOGame();
    }

    public Game getById(int id) {
        return dao.getById(id);
    }

    public ArrayList<GameView> getAll(){

        ArrayList<GameView> gameViews = new ArrayList<GameView>();
        ArrayList<Game> games = new ArrayList<Game>(dao.getAll());

        for(int i = 0; i < games.size(); i++){
            gameViews.add(new GameView(games.get(i)));
        }

        return gameViews;
    }

    public void addGame(Game game){
        dao.add(game);
    }

    public void deleteGame(int id){
        dao.deleteById(id);
    }

}
