package com.example.udesc.gamesmap;

import java.util.Date;

public class Game {
    private String name;
    private String plataforma;
    private String genero;
    private int anoLanc;
    private String empresa;
    private int tamGB;
    private Boolean multiplayer;
    private int ID;

    public Game() {}

    public Game(String name, String plataforma, String genero, int anoLanc, String empresa, int tamGB, Boolean multiplayer, int ID){
        this.name = name;
        this.plataforma = plataforma;
        this.genero = genero;
        this.anoLanc = anoLanc;
        this.empresa = empresa;
        this.tamGB = tamGB;
        this.multiplayer = multiplayer;
        this.ID = ID;
    }

    public Boolean getMultiplayer() {
        return multiplayer;
    }

    public Integer getID() {
        return ID;
    }

    public int getAnoLanc() {
        return anoLanc;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getGenero() {
        return genero;
    }

    public String getName() {
        return name;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public int getTamGB() {
        return tamGB;
    }

    public void setAnoLanc(int anoLanc) {
        this.anoLanc = anoLanc;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setMultiplayer(Boolean multiplayer) {
        this.multiplayer = multiplayer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public void setTamGB(int tamGB) {
        this.tamGB = tamGB;
    }
}
