package com.example.udesc.gamesmap;

import org.json.JSONException;
import org.json.JSONObject;

public class DAOGame extends DAO<Game>{
    @Override
    public String getRepository() {
        return "games/";
    }

    @Override
    public JSONObject getJson(Game object) throws JSONException {
        JSONObject json = new JSONObject();

        json.put("ID", object.getID());
        json.put("name", object.getName());
        json.put("plataforma", object.getPlataforma());
        json.put("genero", object.getGenero());
        json.put("anoLanc", object.getAnoLanc());
        json.put("empresa", object.getEmpresa());
        json.put("tamanho", object.getTamGB());
        json.put("multiplayer", object.getMultiplayer());

        return json;
    }

    @Override
    public Game getObject(JSONObject obj) throws JSONException {
        Game game = new Game();
        game.setID(obj.getInt("id"));
        if(obj.has("name")) game.setName(obj.getString("name"));
        if(obj.has("plataforma")) game.setPlataforma(obj.getString("plataforma"));
        if(obj.has("genero")) game.setGenero(obj.getString("ganero"));
        if(obj.has("anoLanc")) game.setAnoLanc(obj.getInt("anoLanc"));
        if(obj.has("empresa")) game.setEmpresa(obj.getString("empresa"));
        if(obj.has("tamanho")) game.setTamGB(obj.getInt("tamanho"));
        if(obj.has("multiplayer")) game.setMultiplayer(obj.getBoolean("multiplayer"));
        return game;
    }
}
