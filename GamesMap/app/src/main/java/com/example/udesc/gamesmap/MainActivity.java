package com.example.udesc.gamesmap;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private GameService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        service = new GameService();
        LoadAllMovies load = new LoadAllMovies(this);
        load.execute();
    }

    public void pesquisaJogo(View view){
        TextView name = findViewById(R.id.gameName);

    }

    class LoadAllMovies extends AsyncTask<Void, Void, ArrayList<GameView>> {

        @Override
        protected ArrayList<GameView> doInBackground(Void... voids) {
            ArrayList<GameView> movies = service.getAll();
            return movies;
        }
    }


}
