package com.example.udesc.gamesmap;

public class GameView {
    private int id;
    private String name;

    public GameView(){

    }

    public GameView(int id, String name){
        this.id = id;
        this.name = name;
    }

    public GameView(Game game){
        this.id = game.getID();
        this.name = game.getName();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
