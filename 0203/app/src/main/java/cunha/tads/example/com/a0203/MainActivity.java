package cunha.tads.example.com.a0203;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {

    Random rand = new Random();
    private int d = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    /*
    public void abrirToast(View view){
        Toast t = Toast.makeText(this, "ola", Toast.LENGTH_LONG);
        t.show();
    }
    public void executaToast(View view){
        Toast t = Toast.makeText(this,"sei lá",Toast.LENGTH_SHORT);
        t.show();
    }
    */
    public void geraNumero(View view){
        EditText minText = findViewById((R.id.number1));
        String minString = minText.getText().toString();
        int min = Integer.parseInt(minString);

        EditText maxText = findViewById((R.id.number2));
        String maxString = maxText.getText().toString();
        int max = Integer.parseInt(maxString);
        TextView sort = findViewById((R.id.numeroSort));

        if (min > max){
            d =  ThreadLocalRandom.current().nextInt(max, min + 1);
        }
        else{
            d =  ThreadLocalRandom.current().nextInt(min,  max + 1);
        }


        sort.setText(String.valueOf(this.d));

    }

}
