package cunha.tads.example.com.exemplo_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.Random;



public class MainActivity extends AppCompatActivity {
    private int counter = 0;

    Random rand = new Random();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void der(View view){
        TextView t = findViewById((R.id.TextLogin));
        this.counter = rand.nextInt(10);
        //this.counter = (int)(Math.random() * 10 + 1);
        t.setHint("clicks " + this.counter);
    }

}
